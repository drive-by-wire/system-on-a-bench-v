import pygame 
import RPi.GPIO as GPIO
import serial


class CarStates(enum.Enum):
	"""
	class CarStates(enum.Enum):

	Enumeration class for the main vehicle state machine
	"""
	START = enum.auto()
	SETUP = enum.auto()
	OPERATION = enum.auto()
	ERROR = enum.auto()


class InputModes(enum.Enum):
	"""
	class InputModes(enum.Enum):

	Enumeration class for the specific input type
	"""
	SYSTEMOFF = enum.auto()
	JOYSTICK = enum.auto()
	LAPTOP = enum.auto()



class CarValues():
	"""
	class CarValues():

	Defines the parameters needed for vehicle operation in a struct which can be passed around to the various functions that need it
	"""
	def __init__(self):
		self.steering_angle = 0
		self.percent_brake = 0
		self.percent_throttle = 0
		self.horn = 0
		self.reverse = 0
		self.windshield_wiper = 0
		self.E_STOP = 0
		self.left_turn = 0
		self.right_turn = 0
		return


def updateThrottle (current, prev):
	"""
	Commands the accelerator using a DAC
	Author: Arturo Gamboa-Gonzalez

	:param current: container of the new desired values
	:param prev: container of the previous desired values
	:return: success if DAC was connected, failure if disconnnect
	"""
	if ( abs (current.percent_throttle - prev.percent_throttle) > 0.01):
		#set voltage on DAC

	return "success"


def updateBraking (current, prev):
	"""
	Commands the brake actuator to the desired position using a feedback loop
	Author: Arturo Gamboa-Gonzalez

	:param current: container of the new desired values
	:param prev: container of the previous desired values
	:return: success if actuator was connected, failure if disconnnect
	"""
	if ( abs (current.percent_brake - prev.percent_brake) > 0.01):
		#run control loop

	return "success"/"failure"
	


def updateSteering (current, prev):
	"""
	Commands the steering actuator to the desired position using a feedback loop
	Author: Arturo Gamboa-Gonzalez

	:param current: container of the new desired values
	:param prev: container of the previous desired values
	:return: success if actuator was connected, failure if disconnnect
	"""
	if ( abs (current.steering_angle - prev.steering_angle) > 0.01): 
		#run control loop

	return "success"/"failure"



if __name__ == '__main__':
	"""
	Runs the state machine to control car, run at startup
	Authors: Arturo Gamboa-Gonzalez, Sutter Lum, Sriram Venkataraman
	"""

	pygame.init()

	current_state = CarStates.START
	input_mode = InputModes.SYSTEMOFF

	new_vals = CarValues();
	prev_vals = CarValues();

	while (1):

		###########################################################################################
		### START State: Vehicle stays here until a switch is pressed (indicated by input_mode) ###
		###########################################################################################
		
		if (current_state == CarStates.START):
			if (input_mode != InputModes.SYSTEMOFF):
				if (inMotion):
					current_state = CarStates.ERROR
				else:
					current_state = CarStates.START

		################################################################################
		### SETUP State: Perform necessary actions to put vehicle in OPERATION state ###
		################################################################################
		
		elif (current_state == CarStates.SETUP):
			error = 0

			# check power on HV step down circuit, if isolation fault error = 1 
			# check if joystick present in joystick mode, if not present error = 1 
			# check if laptop present in laptop mode, if not present error = 1 
			# run brake actuator test, if no response error = 1
			# run steering actuator test, if no response error = 1
			# run DAC test, if no response error = 1

			if (!error):
				current_state = CarStates.OPERATION
			else:
				current_state = CarStates.ERROR


		#######################################################################################
		### OPERATION State: The vehicle will remain in this state under nominal conditions ###
		#######################################################################################
		
		elif (current_state == CarStates.OPERATION):

			if (input_mode == InputModes.LAPTOP):
				# check if laptop input was received 
					# if not, current_state = CarStates.ERROR
					# break

				# parse serial input & verify checksum (below is just some spitballin)
				buffer = incoming_serial()
				new_vals.percent_brake = buffer[0-1] 
				new_vals.percent_throttle = buffer[2-3] 
				new_vals.steering_angle = buffer[4-5] 
				new_vals.left_turn = buffer[6] 
				new_vals.right_turn = buffer[7]
				new_vals.horn = buffer[8]
				new_vals.windshield_wiper = buffer[9]

			if (input_mode == InputModes.JOYSTICK)
				# check if joystick input was received
					# if not, current_state = CarStates.ERROR
					# break

				#if (Y was pressed):
				#	current_state = CarStates.ERROR
				#	break

				new_vals.percent_brake = left_trigger * scaling 
				new_vals.percent_throttle = right_trigger * scaling 
				new_vals.steering_angle = left_stick * scalar 
				new_vals.left_turn = left_bumper 
				new_vals.right_turn = right_bumper
				new_vals.horn = A
			
			if (updateThrottle(new_vals, prev_vals) == "failure"):
				current_state = CarStates.ERROR
				break
			
			if (updateBraking(new_vals, prev_vals) == "failure"):
				current_state = CarStates.ERROR
				break
			
			if (updateSteering(new_vals, prev_vals) == "failure"):
				current_state = CarStates.ERROR
				break
		
			if (new_vals.left_turn == 1):
				#toggle left turn light

			if (new_vals.right_turn == 1):
				#toggle right turn light

			if (new_vals.horn == 1):
				print("BEEEEEEEEEEEEEEEEP")

			if (new_vals.windshield_wiper == 1):
				#use the windshield wipers

			prev_vals = new_vals

		#########################################################################################
		### ERROR State: The system will remain idle in this state until the reset is pressed ###
		#########################################################################################
		
		elif (current_state == CarStates.ERROR):
			if (reset_was_pressed):
				current_state == CarStates.START
				input_mode = SYSTEMOFF
				new_vals = CarValues();
				prev_vals = CarValues();

		##########################################################
		### Update the tiny display with pertinent information ###
		##########################################################
		
		updateDisplay(current_state, new_vals);



def __ISR__1 (void):
	"""
	ISR to poll for button input
	Author: ?

	:param current: ?
	"""
	if (laptop_GPIO_port == HIGH):
		input_mode = InputModes.LAPTOP
	elif (joystick_GPIO_port == HIGH):
		input_mode = InputModes.JOYSTICK

def __ISR__2 (void):
	"""
	ISR to update the current vehicle speed
	Author: ?

	:param current: ?
	"""
	inMotion = imu reading


def __ISR__3 (void):
	"""
	ISR to check for user takeover from illuminated button, e stop on joystick, brake pedal, and accelerator pedal
	Author: ?

	:param current: ?
	"""
	if (takeover detected):
		current_state = CarStates.ERROR

def __ISR__4 (void):
	"""
	ISR to check if the reset button was pressed
	Author: ?

	:param current: ?
	"""
	if (reset_GPIO_port = HIGH):
		reset_was_pressed = 1