#include "operation.h"
#include "main.h"
#include "CarValues.h"
#include "peripherals.h"
#include "CircularBuffer.h"
#include "PID.h"
#include "Serial.h"
#include "packet_builder.h"
#include <softPwm.h>
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>

#define CLEAR_BUF(buf, len) for(uint16_t i_macro = 0; i_macro < len; i_macro++) buf[i_macro] = 0;
#define PACKET_LEN 12 // Length of the package to be received. TODO: THIS NEEDS TO BE CHANGED
//#define JOYSTICK_SERIAL SD1 // Serial channel for the joystick
//#define JETSON_SERIAL SD2 // Serial channel for the jetson
#define SPI_LEN 2 // SPI Buffer Length

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

// Flags //
static vuint8_t pitFlag; // Timer flag
static vuint8_t isDataReady = 0; // ADC flag
static volatile bool spiFlag; // SPI transmit flag
static volatile bool txFlag; // Serial transmit flag
static volatile bool autonFlag = 0; // Indicates if jetson circular buffer contents are valid
static volatile bool joystickFlag = 0; // Indicates if joystick circular buffer contents are valid
static volatile bool estopPressed = 0; //indicates
//static bool rLight = 1;
//static bool lLight = 1;
//static bool hLight = 1;

// Buffers //
static uint8_t spiBuf[SPI_LEN]; // SPI transmit Buffer
static uint8_t circTemp[PACKET_LEN];
static uint8_t incomingByte[2];
static CircularBuffer joystickCircBuf;
static CircularBuffer jetsonCircBuf;

// pseudo objects? //
static PID steering = NULL;
static PID braking = NULL;


// ADC values //
static uint16_t colADC = 0;
static uint16_t brakeADC0 = 0;
static uint16_t brakeADC1 = 0;

/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

void estop_isr(void){
    printf("Estop rising\r\n");
    if (digitalRead(E_STOP) == HIGH) estopPressed = 1;
    // while(digitalRead(E_STOP) == HIGH){}//printf("Test\r\n");};
}

bool estop_was_pressed(void) {
    return estopPressed;
}

void reset_estop_flag(void) {
    estopPressed = 0;
}

/*
 * Description: Creates the SPI message for the MCP4812 DAC
 * Arguments:
 *   buf - buffer to be written with the message (min len 2)
 *   level - the base 1024 voltage level
 * 
 * Return:
 *   None
 */
void dac_message(uint8_t *buf, uint16_t level){
    level = level & 0x3FF;
    buf[0] = 0x10 + (level >> 6);
    buf[1] = (level << 2) & 0xFF;
}

uint16_t read_adc(uint8_t chan){
    uint8_t buf[3];
    buf[0] = 1;
    buf[1] = (8 + (chan << 4)) & 0xFF;
    buf[2] = 0; 
    wiringPiSPIDataRW(ADC, buf, 3);
    return ((buf[1] & 3) << 8) + buf[2];
}

void refresh_all_adc(void){
    colADC = read_adc(STEERING_COL_CHANNEL);
    if(brakeADC0 == 0) brakeADC0 = read_adc(BRAKE_ACTUATOR_CHANNEL);
    else brakeADC0 = 0.5 * brakeADC0 + 0.5 * read_adc(BRAKE_ACTUATOR_CHANNEL);
    if(brakeADC1 == 0) brakeADC1 = read_adc(BRAKE_PEDAL_CHANNEL);
    else brakeADC1 = 0.5 * brakeADC1 + 0.5 * read_adc(BRAKE_PEDAL_CHANNEL);
    isDataReady = TRUE;

    //printf("%d, %d, %d, %d\n", colADC, brakeADC0, brakeADC1, read_adc(0));
}

bool is_data_ready(void){
    if(isDataReady){
        isDataReady = FALSE;
	return TRUE;
    }
    return isDataReady;
}

void GPIO_init(void){
    wiringPiSetup();
    
    // Mode Select Buttons TODO: Pull up/down if needed
    pinMode(SYS_OFF_BTN, INPUT);
    pullUpDnControl(SYS_OFF_BTN, PUD_DOWN);
    pinMode(JOYSTICK_BTN, INPUT);
    pullUpDnControl(JOYSTICK_BTN, PUD_DOWN);
    pinMode(AUTON_BTN, INPUT);
    pullUpDnControl(AUTON_BTN, PUD_DOWN);

    // Mode Select LEDs
    pinMode(JOYSTICK_LED, OUTPUT);
    digitalWrite(JOYSTICK_LED, LOW);
    pinMode(AUTON_LED, OUTPUT);
    digitalWrite(AUTON_LED, LOW);
    pinMode(SYS_OFF_LED, OUTPUT);
    digitalWrite(SYS_OFF_BTN, LOW);
    pinMode(ALARM, OUTPUT);
    digitalWrite(ALARM, LOW);
    
    pinMode(HEADLIGHT, OUTPUT);
    digitalWrite(HEADLIGHT, HIGH);
    pinMode(LEFT_TURN, OUTPUT);
    digitalWrite(LEFT_TURN, HIGH);
    pinMode(RIGHT_TURN, OUTPUT);
    digitalWrite(RIGHT_TURN, HIGH);
    pinMode(HORN, OUTPUT); 
    digitalWrite(HORN, HIGH);

    pinMode(E_STOP, INPUT);
    pullUpDnControl(E_STOP, PUD_DOWN);
    wiringPiISR(E_STOP, INT_EDGE_RISING, &estop_isr);

    pinMode(GLOBAL_EN, OUTPUT);
    digitalWrite(GLOBAL_EN, HIGH);
    
    
    wiringPiSPISetup(ADC, 1000000);
    wiringPiSPISetup(DAC, 1000000);
    
    softPwmCreate(PWM0, 0, PWM_RANGE);
    softPwmCreate(PWM1, 0, PWM_RANGE);
    softPwmCreate(PWM2, 0, PWM_RANGE);
    softPwmCreate(PWM3, 0, PWM_RANGE);
}

/*
 * Description: Commands the accelerator using a DAC
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *   SUCCESS if SPI channel available, FAILURE if SPI channel occupied
 */
bool update_throttle(CarValues *current) {
    // Off voltage level is 244, MAX voltage level is 

	//if(!spiFlag) return FAILURE; //TODO: on SPC58
	//spiFlag = false;
	dac_message(spiBuf, current->percentThrottle * 10); // THIS VALUE IS INCORRECT. TODO: CALCULATE SCALING
	//spi_lld_send(&SPID1, 2, spiBuf); // TODO
	wiringPiSPIDataRW(DAC, spiBuf, 2);
	return SUCCESS;
}

/*
 * Description: Commands the brake actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  SUCCESS if actuator is connected, FAILURE if disconnnected
 */
bool update_braking(CarValues *current) {
    int8_t control;
    PID_setPosition(braking, 334 - (current->percentBrake * 32 / 10));
    control = PID_update(braking, brakeADC0);
    
    //printf("CMD: %d, Pos: %d, u0: %d\r\n", 334 - (current->percentBrake * 32 / 10), brakeADC0, control);
    if (control > 10){
        softPwmWrite(PWM3, 0);
	    softPwmWrite(PWM2, control / 3);
    }
    else if (control < -10){
        softPwmWrite(PWM2, 0);
	    softPwmWrite(PWM3, abs(control) / 3);
    }
    else{
        softPwmWrite(PWM2, 0);
        softPwmWrite(PWM3, 0);
    }
    return SUCCESS;
    //return control;
}

/*
 * Description: Commands the steering actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 * Return:
 *   SUCCESS if actuator is connected, FAILURE if disconnnected
 */
bool update_steering(CarValues *current) {
    // printf("steer: %d, throttle: %d, brake: %d\r\n", current->steeringAngle, current->percentThrottle, current->percentBrake);
			
    // TODO: Calculate range of values for 0-344
    int8_t control;
    PID_setPosition(steering, (current->steeringAngle) /  6 + 104);
    control = PID_update(steering, colADC);
    
    //printf("CMD: %d, Pos: %d, u0: %d\r\n", (current->steeringAngle) / 6 + 104, colADC, control);
    if (control > 5){
        softPwmWrite(PWM1, 0);
	softPwmWrite(PWM0, control);
    }
    else if (control < -5){
        softPwmWrite(PWM0, 0);
        softPwmWrite(PWM1, abs(control));
    }
    else{
        softPwmWrite(PWM0, 0);
        softPwmWrite(PWM1, 0);
    }
    
    //return control;
    return SUCCESS;
}

/*
 * Description: Controlling the peripherals TODO
 * Arguments:
 *   current - container of the new desired values
 *
 * Return:
 *  NULL
 */
void update_peripherals(CarValues *current, CarValues *previous) {
    (void)previous;
    //if(current->leftTurn && !previous->leftTurn) lLight = !lLight;
    //if(current->rightTurn && !previous->rightTurn) rLight = !rLight;
    //if(current->headlights && !previous->headlights) hLight = !hLight;
    digitalWrite(LEFT_TURN, !previous->leftTurn);
    digitalWrite(RIGHT_TURN, !previous->rightTurn);
    digitalWrite(HEADLIGHT, !previous->headlights);
    digitalWrite(HORN, !current->horn);
    /*
    // toggle left turn light on button down event TODO
    if (current->leftTurn == True && prev->leftTurn == False) pal_lld_togglepad(LEFT_TURN_LED_PORT, LEFT_TURN_LED_PIN);

    // toggle right turn light on button down event TODO
    if (current->rightTurn == True && prev->rightTurn == False) pal_lld_togglepad(RIGHT_TURN_LED_PORT, RIGHT_TURN_LED_PIN);

    // Set GPIO for horn TODO
    if (current->horn == True) pal_lld_setpad(HORN_PORT, HORN_PIN);
    else pal_lld_clearpad(HORN_PORT, HORN_PIN);

    // toggle headlights on button down event TODO
    if (current->headlights == True && prev->headlights == False) pal_lld_togglepad(HEADLIGHT_PORT, HEADLIGHT_PIN);
    */
    return;
}

// TODO
int8_t autonomous_read(CarValues *newVals, CircularBuffer circBuf) {
    int8_t temp = Serial_update();
    if (temp < 0) return temp;
    while(!Serial_isRxEmpty()){
        Serial_read(incomingByte, 1);
        CircularBuffer_append(circBuf, incomingByte, 1);
        if(CircularBuffer_getLen(circBuf) == PACKET_LEN && CircularBuffer_verifyPacket(circBuf)){
            autonFlag = 1;
            return 1;

        }
    }
    return 0;
}

// TODO: Add checksum verification
int8_t joystick_read(CarValues *newVals, CircularBuffer circBuf) {
    int8_t temp = Serial_update();
    if (temp < 0) return temp;
    while(!Serial_isRxEmpty()){
        Serial_read(incomingByte, 1);
        CircularBuffer_append(circBuf, incomingByte, 1);
        if(CircularBuffer_getLen(circBuf) == PACKET_LEN && CircularBuffer_verifyPacket(circBuf)){
            joystickFlag = 1;
            return 1;
        //     //for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
        //     if (CircularBuffer_verifyPacket(circBuf)){
        //         CircularBuffer_getBuf(circBuf, circTemp, PACKET_LEN);
		// if (packet2values(circTemp, newVals) > CHECKSUM_ERROR_LENGTH) {
		// 	//currentState = MID_OPERATION_ERROR;
		// 	printf("MID-OPERATION ERROR OCCURED\r\n");
		// 	for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
		// 	printf("\r\n");
		// 	//global_enable_off();
		// 	//play_error_tone();
		// 	//system_off_LED();
		// 	//CircularBuffer_clear(joystickCircBuf);
		// 	return -1;
		// }
		// else{
		// 	// printf("steer: %d, throttle: %d, brake: %d\r\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);
		// 	//for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
		// 	//printf("\r\n");
		// 	CircularBuffer_clear(circBuf);
		// 	joystickFlag = 1;
		// 	return 1;
		// }
        //     }


        }
    }
    return 0;
}



// TODO: Add checksum verification
int8_t sriram_read(CarValues *newVals, CircularBuffer circBuf) {
    int8_t temp = Serial_update();
    if (temp < 0) return temp;
    while(!Serial_isRxEmpty()){
        Serial_read(circTemp, 1);
        CircularBuffer_append(circBuf, circTemp, 1);
        if(CircularBuffer_getLen(circBuf) == PACKET_LEN){
            //for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
            if (CircularBuffer_verifyPacket(circBuf)){
                CircularBuffer_getBuf(circBuf, circTemp, PACKET_LEN);
		if (packet2values(circTemp, newVals) > CHECKSUM_ERROR_LENGTH) {
			//currentState = MID_OPERATION_ERROR;
			printf("MID-OPERATION ERROR OCCURED\r\n");
			for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
			printf("\r\n");
			//global_enable_off();
			//play_error_tone();
			//system_off_LED();
			//CircularBuffer_clear(joystickCircBuf);
			return -1;
		}
		else{
			// printf("steer: %d, throttle: %d, brake: %d\r\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);
			//for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
			//printf("\r\n");
			CircularBuffer_clear(circBuf);
			joystickFlag = 1;
			return 1;
		}
            }
        }
    }
    return 0;
}


bool get_autonomous_flag(void) {
    if (autonFlag) {
	autonFlag = 0;
	return 1;
    }
    return 0;
}
bool get_joystick_flag(void) {
    if(joystickFlag){
	    joystickFlag = 0;
	    return 1;
    }
    return 0;
}


void init_communication_buffers(void) {
    jetsonCircBuf = CircularBuffer_init();
    joystickCircBuf = CircularBuffer_init();
}

void init_PID_loops(void){
    if(steering != NULL)
        PID_delete(&steering);
    if(braking != NULL)
        PID_delete(&braking);

    steering = PID_init(10, 1, 0.2, TS, colADC, SATURATION >> 2);
    PID_setPosition(steering, colADC);
    braking = PID_init(10, 1, 0.2, TS, brakeADC0, 300);
    PID_setPosition(braking, brakeADC0);
}


uint8_t manual_override_detected(void){
    return abs((brakeADC0 * 82 / 100 + 90) - brakeADC1) > 100;
}

uint16_t get_brake0(void){
    return brakeADC0;
}
uint16_t get_brake1(void){
    return brakeADC1; 
}

uint16_t get_steering_pos(void){
    return colADC;
}