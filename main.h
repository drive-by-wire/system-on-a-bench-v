/*
 * main.h
 *
 *  Created on: Apr 13, 2022
 *      Author: artur
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdlib.h>
#include <stdint.h>

/*
*  Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
*  Description: Header file for the HSL Drive-by-Wire Senior Design Project
*/

/* ============================================================================
 *                           DEFINES SECTION
 * ==========================================================================*/

#define RX_LEN 20

#define FAILURE 0
#define SUCCESS 1

#define False 0
#define True 1

// Global Enable
#define GLOBAL_EN 26

// MODE SELECT GPIO
#define SYS_OFF_BTN 3
#define JOYSTICK_BTN 2
#define AUTON_BTN 0
#define SYS_OFF_LED 7
#define JOYSTICK_LED 9
#define AUTON_LED 8
#define ALARM 21

// Peripherals
#define HEADLIGHT 4
#define LEFT_TURN 5
#define RIGHT_TURN 6
#define HORN 29

// E-Stops
#define E_STOP 1

// PWM Channels
#define PWM0 22
#define PWM1 23
#define PWM2 24
#define PWM3 25

// SPI Channels
#define ADC 0
#define DAC 1

/* ============================================================================
 *                           ENUMS SECTION
 * ==========================================================================*/

typedef enum {
    IDLE,                   // Vehicle stays here until a switch is pressed (indicated by input_mode)
    SETUP,                  // Perform necessary actions to put vehicle into SOFTWARE_CONTROL
    SOFTWARE_CONTROL,       // The vehicle will remain in this state under nominal conditions
    MANUAL_CONTROL,         // The system will remain idle in this state until the reset is pressed
    STARTUP_ERROR,           // Error state for when startup error occurs
    MID_OPERATION_ERROR,    // Error state for when mid-operation error occurs
    E_STOP_ERROR            // Error state for when E-Stop is pressed
} CarState;

typedef enum {
    SYSTEMOFF,              // Software commands aren't controlling the vehicle
    GAMEPAD,                // Gamepad input has highest priority
    AUTONOMOUS              // Autonomous controller has the highest priority
} InputMode;

typedef volatile uint8_t vuint8_t;

typedef volatile uint16_t vuint16_t;

/* ============================================================================
 *                        CLASS DEFINITION SECTION
 * ==========================================================================*/




#endif /* MAIN_H_ */
