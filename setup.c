#include "setup.h"
#include "main.h"
#include "CarValues.h"
#include "peripherals.h"
#include "operation.h"
#include "PID.h"
#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>
#include <wiringPiSPI.h>


#define TICKS_PER_ROT 819 // Number of ADC ticks per rotation of the steering column
#define TICK_PERIOD 100 //10 kHz for PWM
#define DELAY 0 // PWM Delay
#define TRIGGER 0 // PWM Trigger
#define CH0 0 // PWM Channel 0
#define CH1 1 // PWM Channel 1
#define CH2 2 // PWM Channel 2
#define CH3 3 // PWM Channel 3
#define ACCEL_OFF 245
#define SPI_LEN 2

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

// Flags //
static vuint8_t pitFlag; // Timer flag
static vuint8_t isDataReady; // ADC flag
static volatile bool spiFlag; // SPI transmit flag

// ADC variables //
static vuint16_t accelCheck;
static vuint16_t colADC; // ADC channel 63
static vuint16_t brakeADC0; // ADC channel 60
static vuint16_t brakeADC1; // ADC channel 62

/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

/*
 * Description: Verify that the brake actuator & brake angle sensor are connected
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool brake_test(void) {
	//display(starting braking actuator test)
	//move linear actuator to 50% extension
	// if brake angle isn't ~50%, return False
	//move linear actuator to 100% extension
	// if brake angle isn't ~100%, return False
	//return linear actuator to default position
	uint32_t curTime;
	uint32_t endTime;
	CarValues* vals = car_values_init();
	uint16_t pos = 334 - 50 * 32 / 10;
	vals->percentBrake = 50;

	endTime = millis() + 4000;
	curTime = millis();

	while(millis() < endTime){
		if(curTime + 50 <= millis()){
			refresh_all_adc();
			//printf("%d\n", get_brake0());
			update_braking(vals);
			curTime = millis();
		}
	}

	// Disable PWM channels
	softPwmWrite(PWM2, 0);
	softPwmWrite(PWM3, 0);

	// Wait for new ADC readings
	refresh_all_adc();
	if(get_brake0() > pos * 12 / 10 || get_brake0() < pos * 8 / 10) return FAILURE;

	vals->percentBrake = 75;
	pos = 334 - 75 * 32 / 10;
	endTime = millis() + 4000;

	while(millis() < endTime){
		if(curTime + 50 <= millis()){
			refresh_all_adc();
			//printf("%d\n", get_brake0());
			update_braking(vals);
			curTime = millis();
		}
	}

	// Disable PWM channels
	softPwmWrite(PWM2, 0);
	softPwmWrite(PWM3, 0);

	// Wait for new ADC readings
	refresh_all_adc();

	if(get_brake0() > pos * 12 / 10 || get_brake0() < pos * 8 / 10) return FAILURE;
	return SUCCESS;
}


/*
 * Description: Verify that the steering actuator & steering angle sensor are connected
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool steering_test(void) {
	// display (starting steering actuator test)
	// move steering angle to 5 degrees
	// if steering angle error is >10%, return False
	// move steering angle to -5 degrees
	// is steering angle error is >10%, return False
	// return steering angle to default position
	uint32_t curTime;
	uint32_t endTime;
	CarValues* vals = car_values_init();
	uint16_t pos = 2500 /  6 + 104;
	vals->steeringAngle = 2500;

	endTime = millis() + 3000;
	curTime = millis();

	while(millis() < endTime){
		if(curTime + 50 <= millis()){
			refresh_all_adc();
			update_steering(vals);
			curTime = millis();
		}
	}

	// Disable PWM channels
	softPwmWrite(PWM0, 0);
	softPwmWrite(PWM1, 0);

	// Wait for new ADC readings
	refresh_all_adc();
	if(get_steering_pos() > pos * 11 / 10 || get_steering_pos() < pos * 9 / 10) return FAILURE;

	vals->steeringAngle = 3750;
	pos = 3750 /  6 + 104;
	endTime = millis() + 3000;

	while(millis() < endTime){
		if(curTime + 50 <= millis()){
			refresh_all_adc();
			update_steering(vals);
			curTime = millis();
		}
	}

	// Disable PWM channels
	softPwmWrite(PWM0, 0);
	softPwmWrite(PWM1, 0);

	// Wait for new ADC readings
	refresh_all_adc();

	if(get_steering_pos() > pos * 11 / 10 || get_steering_pos() < pos * 9 / 10) return FAILURE;

	return SUCCESS;
}


/*
 * Description: Verify that the DAC isn't fried
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool DAC_test(void) {
	//command a voltage on the DAC
	//if the voltage read from ADC varies by 10%, return FALSE

	/*while(!spiFlag);
	spiFlag = FALSE;
	dac_message(spiBuf, ACCEL_OFF);
	spi_lld_send(&SPID1, 2, spiBuf);
	delay(10);
	isDataReady = 0;
	while(!isDataReady);
	accelCheck = accelCheck >> 2;
	if(accelCheck < ACCEL_OFF - (ACCEL_OFF / 10) || accelCheck > ACCEL_OFF + (ACCEL_OFF / 10))
		return FALSE;*/
	uint8_t spiBuf[] = {0, 0};
	uint16_t accel;
	return true; // TODO
	for(uint16_t i = 100; i < 245; i+=1){
		dac_message(spiBuf, i);
		wiringPiSPIDataRW(DAC, spiBuf, 2);
		accel = read_adc(0);
		accel = read_adc(0);
		printf("%d, %d\n", accel * 148 / 100 - 11, i);
		if(abs(accel * 148 / 100 - 11 - i) > 16){
		 	return FAILURE;
		}
	}
	return TRUE;
}

