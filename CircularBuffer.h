// Written by Sutter Lum & Arturo Gamboa-Gonzales
// Last updated: 5/07/2022

#ifndef INCLUDE_CIRCULARBUFFER_H_
#define INCLUDE_CIRCULARBUFFER_H_

#include <stdint.h>

#define CIRCULAR_LEN 12 // 1 + # of bytes that our buffer can store - each packet is 12 bytes
#define HEAD_ID 222 //8-bit number identifying start of message - 0xDE in hex
#define TAIL_ID 0 //8-bit number identifying end of message - 0x00 in hex
#define PACKET_LEN 12 // Length of the package to be received. TODO: THIS NEEDS TO BE CHANGED
#define CHECKSUM_ERROR_LENGTH 99 // any number above this indicates a checksum error

typedef struct CircularBufferObj* CircularBuffer;

/*
 * Description: Initializes the Circular Buffer
 * Arguments:
 *   None
 * Return:
 *   None
 */
CircularBuffer CircularBuffer_init(void);

/*
 * Description: Appends n number of bits to the circular buffer,
 *				overwriting any previous values
 * Arguments:
 *   circle - the circular buffer being appended to
 *   buf - The buffer to copy from
 *	 len - The number of bytes to store. Any length over CIRCULAR_LEN
 *         will cause some values to be overwritten
 * Return:
 *   The number of bytes successfully stored
 */
uint16_t CircularBuffer_append(CircularBuffer circle, uint8_t *buf, uint16_t len);

/*
 * Description: Get the number of bytes in the buffer
 * Arguments:
 *   circle - the circular buffer being measured
 * Return:
 *   The number of bytes in the circular buffer
 */
uint16_t CircularBuffer_getLen(CircularBuffer circle);

/*
 * Description: Copy the circular buffer to an array
 * Arguments:
 *   circle - the circular buffer being copied
 *   buf - The buffer to copy to
 *	 len - The number of bytes to copy. If len > CIRCULAR_LEN,
 *         the resuling array will repeat.
 * Return:
 *   The number of bytes successfully copied
 */
uint16_t CircularBuffer_getBuf(CircularBuffer circle, uint8_t *buf, uint16_t len);

/*
 * Description: Reset the circular buffer
 * Arguments:
 *   circle - the circular buffer being reset
 * Return:
 *   None
 */
void CircularBuffer_clear(CircularBuffer circle);

/*
 * Description: Check if current contents of buffer are a valid packet structure
 * Arguments:
 *   circle - the circular buffer being checked
 * Return:
 *   1 if buffer contains valid structure
 *   0 if invalid
 */
uint16_t CircularBuffer_verifyPacket(CircularBuffer circle);

void CircularBuffer_print(CircularBuffer circle);

/*
 * Description: Frees the allocated PID object
 * Arguments:
 *   p - pointer to the PID pointer to be freed
 * Return:
 *   None
 */
void CircularBuffer_delete(CircularBuffer* p);

#endif /* INCLUDE_CIRCULARBUFFER_H_ */
