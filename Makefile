Drive_by_Wire: Drive_by_Wire.o Serial.o operation.o CircularBuffer.o CarValues.o PID.o crc32.o packet_builder.o setup.o
	gcc -std=c11 -lwiringPi -lpthread -lm -Wall -o Drive_by_Wire Drive_by_Wire.o Serial.o operation.o CircularBuffer.o CarValues.o PID.o crc32.o packet_builder.o setup.o

Drive_by_Wire.o: Drive_by_Wire.c
	gcc -std=c11 -lwiringPi -lpthread -lm -Wall -c Drive_by_Wire.c Serial.h main.h operation.h CircularBuffer.h CarValues.h LEDs.h setup.h

Serial.o: Serial.c
	gcc -std=c11 -lwiringPi -Wall -c Serial.c Serial.h
	
CircularBuffer.o: CircularBuffer.c
	gcc -std=c11 -Wall -c CircularBuffer.c CircularBuffer.h
	
operation.o: operation.c
	gcc -std=c11 -lwiringPi -lpthread -Wall -c operation.c operation.h CarValues.h main.h peripherals.h CircularBuffer.h PID.h Serial.h packet_builder.h

CarValues.o: CarValues.c
	gcc -std=c11 -lwiringPi -Wall -c CarValues.c CarValues.h main.h

setup.o: setup.c
	gcc -std=c11 -lwiringPi -lpthread -Wall -c setup.c setup.h PID.h main.h CarValues.h peripherals.h operation.h

PID.o: PID.c
	gcc -std=c11 -Wall -c PID.c PID.h

crc32.o: crc32.c
	gcc -std=c11 -Wall -c crc32.c crc32.h

packet_builder.o: packet_builder.c
	gcc -std=c11 -Wall -c packet_builder.c packet_builder.h crc32.h main.h CarValues.h


SerialTest : SerialTest.o
	gcc -std=c11 -lwiringPi -Wall -o SerialTest SerialTest.o

SerialTest.o : SerialTest.c
	gcc -std=c11 -lwiringPi -Wall -c SerialTest.c

Serial2 : Serial2.o
	gcc -std=c11 -lwiringPi -Wall -o Serial2 Serial2.o

Serial2.o : Serial2.c
	gcc -std=c11 -lwiringPi -Wall -c Serial2.c

clean:
	rm -f *.o *.gch Drive_by_Wire Serial SerialTest Serial2
