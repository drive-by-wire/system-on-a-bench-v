/*
 * CarValues.c
 *
 *  Created on: May 8, 2022
 *      Author: spcstudio
 */

#include <stdlib.h>
#include "CarValues.h"
#include "main.h"


CarValues* car_values_init(void) {
    CarValues* newCarValues  = malloc(sizeof(CarValues));
    newCarValues->steeringAngle = 2500;
    newCarValues->percentBrake = 0;
    newCarValues->percentThrottle = 0;
    newCarValues->horn = False;
    newCarValues->reverse = False;
    newCarValues->headlights = False;
    newCarValues->ESTOP = False;
    newCarValues->leftTurn = False;
    newCarValues->rightTurn = False;
    return newCarValues;
}

void car_values_reset(CarValues *curr) {
    curr->steeringAngle = 0;
    curr->percentBrake = 0;
    curr->percentThrottle = 0;
    curr->horn = False;
    curr->reverse = False;
    curr->headlights = False;
    curr->ESTOP = False;
    curr->leftTurn = False;
    curr->rightTurn = False;
}

void car_values_overwrite(CarValues *prev, CarValues *curr) {
    prev->steeringAngle = curr->steeringAngle;
    prev->percentBrake = curr->percentBrake;
    prev->percentThrottle = curr->percentThrottle;
    prev->horn = curr->horn;
    prev->reverse = curr->reverse;
    prev->headlights = curr->headlights;
    prev->ESTOP = curr->ESTOP;
    prev->leftTurn = curr->leftTurn;
    prev->rightTurn = curr->rightTurn;
}