#ifndef INCLUDE_SETUP_H_
#define INCLUDE_SETUP_H_

#include <stdbool.h>

bool brake_test(void);
bool steering_test(void);
bool DAC_test(void);

#endif /* INCLUDE_SETUP_H_ */
