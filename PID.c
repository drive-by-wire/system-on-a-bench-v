/*
 * PID.c
 *
 *  Created on: Apr 18, 2022
 *      Author: artur
 *
 *      TODO: have all the calculations be done using integers
 */

#include "PID.h"
#include <stdlib.h>


typedef struct PIDObj{
	float k_p;
	float k_i;
	float k_d;
	float accumulator; // Accumulator
	int8_t control; // Current Control effort
	uint16_t prevPos; // Previous position
	uint16_t cmdPos; // Current commanded position
	uint16_t saturation; // Actuator saturation limit in position units
	uint16_t ts; // Time step in milliseconds
}PIDObj;


/*
 * Description: Initializes a PID object
 * Arguments:
 *   p - Proportional Gain
 *	 i - Integral Gain
 *   d - Derivative Gain
 *   ts - Time step
 *   currentPos - current plant position in ticks
 *   saturation - actuator control effort saturation limit in ticks
 * Return:
 *   A pointer to a PID object
 */
PID PID_init(float k_p, float k_i, float k_d, uint16_t ts, uint16_t currentPos, uint16_t saturation){
	PID pid = (PID)malloc(sizeof(PIDObj));

	pid->k_p = k_p;
	pid->k_i = k_i;
	pid->k_d = k_d;
	pid->accumulator = 0;
	pid->control = 0;
	pid->prevPos = currentPos;
	pid->cmdPos = currentPos;
	pid->saturation = saturation;
	pid->ts = ts;
	return pid;
}

/*
 * Description: Updates the commanded position
 * Arguments:
 *   pid - the PID object to update
 *   command - the desired position in ticks
 * Return:
 *   None
 */
void PID_setPosition(PID pid, uint16_t command){
	pid->cmdPos = command;
}

/*
 * Description: Performs the digital PID calculations to get
 * 				the actuator control effort
 * Arguments:
 *   pid - the PID object to update
 *   curPos - the current plant position in ticks
 * Return:
 *   the control effort for that time step
 */
int8_t PID_update(PID pid, uint16_t curPos){
	float u_p; // Proportional controller
	float u_i; // Integral controller
	float u_d; // Derivative Controller
	float a0; // Current accumulator
	float u0;
	int16_t err;

	err = pid->cmdPos - curPos;
	u_p = pid->k_p * (float)err;
	u_d = pid->k_d * (float)(pid->prevPos - curPos) * 1000 / pid->ts;
	a0 = pid->accumulator + (float)err * pid->ts / 1000;
	u_i = pid->k_i * a0;
	u0 = u_p + u_i + u_d;
	if(abs(u0) > pid->saturation){
	  a0 = pid->accumulator;
	  u_i = 0;
	  u0 = (u0 > 0)? pid->saturation : -pid->saturation;
	}

	pid->accumulator = a0;
	pid->prevPos = curPos;
	pid->control = (int8_t)(u0 * 100 / pid->saturation);

	return pid->control;
}

/*
 * Description: Get the current control effort
 * Arguments:
 *   pid - the PID object to read from
 * Return:
 *   The current control effort
 */
int8_t PID_getControlEffort(PID pid){
	return pid->control;
}

/*
 * Description: Frees the allocated PID object
 * Arguments:
 *   p - pointer to the PID pointer to be freed
 * Return:
 *   None
 */
void PID_delete(PID* p){
	if( p!=NULL && *p!=NULL ){
		free(*p);
		*p = NULL;
	}
}
