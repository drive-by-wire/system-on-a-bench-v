// Written by Sutter Lum
// Last updated: 5/07/2022
#include <stdint.h>
#include <stdio.h>
#include "crc32.h"
#include "main.h"
#include "CarValues.h"

/*
 * Description: Converts a set of car values into a packet for transmission
 * Arguments:
 *   packet - a pointer to an empty packet
 *   cmd - a CarValues object containg the desired state of the car
 *   errorCode - the error code to be passed in
 * Return:
 *   None
 */
void cmd2packet(uint8_t *packet, CarValues *cmd, uint8_t errorCode);

/*
 * Description: Converts a packet into a set of carValues
 * Arguments:
 *   *packet - a pointer to the packet being decoded
 *   curValues - a CarValues object that the result is stored in
 * Return:
 *   ErrorCode, if it's >100, a checksum error occured
 */
uint8_t packet2values(uint8_t *packet, CarValues *curValues);
