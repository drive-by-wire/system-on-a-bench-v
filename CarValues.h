/*
 * CarValues.h
 *
 *  Created on: May 8, 2022
 *      Author: spcstudio
 */

#ifndef INCLUDE_CARVALUES_H_
#define INCLUDE_CARVALUES_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct CarValues{
        uint16_t steeringAngle;
        uint8_t percentBrake;
        uint8_t percentThrottle;
        bool horn;
        bool reverse;
        bool headlights;
        bool ESTOP;
        bool leftTurn;
        bool rightTurn;

} CarValues;

CarValues* car_values_init(void) ;
void car_values_reset(CarValues *curr);
void car_values_overwrite(CarValues *prev, CarValues *curr);


#endif /* INCLUDE_CARVALUES_H_ */
