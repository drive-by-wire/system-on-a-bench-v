/*
 * peripherals.h
 *
 *  Created on: May 1, 2022
 *      Author: spcstudio
 */

#ifndef INCLUDE_PERIPHERALS_H_
#define INCLUDE_PERIPHERALS_H_

#include <stdint.h>

#define LEFT_TURN_LED_PORT 1
#define LEFT_TURN_LED_PIN 1

#define RIGHT_TURN_LED_PORT 1
#define RIGHT_TURN_LED_PIN 1

#define HORN_PORT 1
#define HORN_PIN 1

#define HEADLIGHT_PORT 1
#define HEADLIGHT_PIN 1

#endif /* INCLUDE_PERIPHERALS_H_ */
