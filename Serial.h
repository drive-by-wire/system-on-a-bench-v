/*
 * Serial.h
 * 
 * Created on: May 16, 2022
 *      Author: Arturo
 * 
 * Adapted from CSE121 with Prof. Varma
 */

#ifndef INCLUDE_SERIAL_H_
#define INCLUDE_SERIAL_H_

#include <stdint.h>

#define BAUDRATE B115200

/*
 * Description: Initializes the Serial Channel. wiringPiSetup() MUST be 
 *              called before this function.
 * Arguments:
 *   None
 * Return:
 *   1 for success, -1 for error
 */
int8_t Serial_init(void);

/*
 * Description: Updates the RX buffer and returns the number of bytes 
 *              available to be read
 * Arguments:
 *   None
 * Return:
 *   Number of bytes available to read, or -1 for an invalid read.
 */
int8_t Serial_isRxEmpty(void);


int8_t Serial_update(void);

/*
 * Description: Transfers characters from the RX buffer to the the buffer param
 * Arguments:
 *   buffer - the buffer to store the received bytes
 *   size - The number of bytes to read
 * Return:
 *   The number of bytes successfuly read
 */
int8_t Serial_read(uint8_t* buffer, uint8_t size);

/*
 * Description: Uses the TX Serial channel to send bytes out
 * Arguments:
 *   buffer - the buffer of bytes to transmit
 *	 size - The number of bytes to transmit
 * Return:
 *   The number of bytes successfully transmitted, or -1 if error
 */
int8_t Serial_write(uint8_t* buffer, uint8_t size);

int8_t Serial_write_null(uint8_t* buffer, uint8_t size);


#endif
