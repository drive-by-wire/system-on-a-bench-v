/************************
 * This file reads in a value from the RX buffer
 * and then transmits it back out through the TX output pin
 ***********************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <wiringPi.h>

#define BAUDRATE B38400 // UART speed

int main(int argc, char * argv[]){
	wiringPiSetup();
	
	struct termios serial; // Structure to contain UART parameters

	char* dev_id = "/dev/serial0"; // UART device identifier
	char rxbuffer[100]; // Receive data buffer

	printf("Opening %s\n", dev_id);
	int fd = open(dev_id, O_RDWR | O_NOCTTY | O_NDELAY);
	// O_RDWR indicates the device needs to both written and read.
	// O_NOCTTY states that the device is not used as a console.
	// O_NDELAY indicates that read and writes should be nonblocking.

	if (fd == -1){ // Open failed
		perror(dev_id);
		return -1;
	}

	// Get UART configuration
	if (tcgetattr(fd, &serial) < 0){
		perror("Getting configuration");
		return -1;
	}

	// Set UART parameters in the termios structure
	serial.c_iflag = 0;
	serial.c_oflag = 0;
	serial.c_lflag = 0;
	serial.c_cflag |= PARENB; //BAUDRATE | CS8 | PARENB;// | PARODD;
	// Speed setting + 8-bit data + Enable RX + Enable Parity + Odd Parity
  
	serial.c_cc[VMIN] = 0; // 0 for Nonblocking mode
	serial.c_cc[VTIME] = 0; // 0 for Nonblocking mode
	
	// Set the parameters by writing the configuration
	tcsetattr(fd, TCSANOW, &serial);

	char temp[] = "23";
	
	for(;;){
		// Gets number of bytes read
		int read_bytes = read(fd, rxbuffer, sizeof(rxbuffer));
		// if byte is received but with error, terminate program
		if (read_bytes < 0){
			perror("Read");
			return -1;
		}
		// If read bytes
		//if(read_bytes > 0){
			// assign bytes to TX output and get error status
			int wcount = write(fd, temp, sizeof(temp));
			// if error terminate program
			if (wcount < 0){
				perror("Write");
				return -1;
			}
			delay(500);
		//}
	}
	
	close(fd); // Close RX file
}
