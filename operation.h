#ifndef INCLUDE_OPERATION_H_
#define INCLUDE_OPERATION_H_

#include <stdint.h>
#include <stdbool.h>
#include "CarValues.h"
#include "CircularBuffer.h"

#define STEERING_COL_CHANNEL 5
#define BRAKE_PEDAL_CHANNEL 7 	 // TODO UPDATE WITH ACTUAL CHANNEL
#define BRAKE_ACTUATOR_CHANNEL 6 // TODO UPDATE WITH ACTUAL CHANNEL
#define HUMAN_INPUT_THRESHOLD 69 // TODO UPDATE WITH EXPERIMENTALLY DETERMINED VALUE
#define STEERING_INPUT_THRESHOLD 250 //TODO REPLACE
#define BRAKING_INPUT_THRESHOLD 69
#define THROTTLE_INPUT_THRESHOLD 69
#define PWM_RANGE 100

void dac_message(uint8_t *buf, uint16_t level); // Should this be private?

// ADC FUNCTIONS //
uint16_t read_adc(uint8_t chan);
void refresh_all_adc(void);
bool is_data_ready(void);

void GPIO_init(void);

bool update_throttle(CarValues *current);
bool update_braking(CarValues *current);
bool update_steering(CarValues *current);
void update_peripherals(CarValues *current, CarValues *previous);
int8_t autonomous_read(CarValues *newVals, CircularBuffer circBuf);
int8_t joystick_read(CarValues *newVals, CircularBuffer circBuf);
bool get_autonomous_flag(void);
bool get_joystick_flag(void);
void init_communication_buffers(void);
void init_PID_loops(void);

uint8_t manual_override_detected(void);
uint16_t get_brake0(void); 
uint16_t get_brake1(void);
uint16_t get_steering_pos(void);

bool estop_was_pressed(void);
void reset_estop_flag(void);

#endif /* INCLUDE_OPERATION_H_ */
