// Written by Sutter Lum & Arturo Gamboa-Gonzales
// Last updated: 5/07/2022

#include "CircularBuffer.h"
#include <stdlib.h>
#include <stdio.h>


typedef struct CircularBufferObj{
	uint8_t buffer[CIRCULAR_LEN];
	uint16_t readPtr;
	uint16_t writePtr;
	uint16_t size;
}CircularBufferObj;



CircularBuffer CircularBuffer_init(void){
	CircularBuffer circle = (CircularBuffer)malloc(sizeof(CircularBufferObj));
	uint16_t i;
	circle->readPtr = 0;
	circle->writePtr = 0;
	circle->size = 0;
	for(i = 0; i < CIRCULAR_LEN; i++)
		circle->buffer[i] = 0;
	return circle;
}



uint16_t CircularBuffer_append(CircularBuffer circle, uint8_t *buf, uint16_t len){
	uint16_t i;
	for(i = 0; i < len; i++){
		circle->buffer[circle->writePtr] = buf[i];
		(circle->writePtr)++;
		if(circle->size < CIRCULAR_LEN) (circle->size)++;
		if(circle->writePtr >= CIRCULAR_LEN)
			circle->writePtr = 0;
	}
	if(circle->size >= CIRCULAR_LEN) circle->readPtr = circle->writePtr;
	return (i > CIRCULAR_LEN)? CIRCULAR_LEN : i;
}


uint16_t CircularBuffer_getLen(CircularBuffer circle){
	return circle->size;
}


uint16_t CircularBuffer_getBuf(CircularBuffer circle, uint8_t *buf, uint16_t len){
	uint16_t i, j;
	for(i = 0, j = circle->readPtr; i < len; i++){
		buf[i] = circle->buffer[j];
		j++;
		if(j >= CIRCULAR_LEN)
			j = 0;
	}
	return i;
}


void CircularBuffer_clear(CircularBuffer circle){
	uint16_t i;
	for(i = 0; i < CIRCULAR_LEN; i++)
		circle->buffer[i] = 0;
	circle->readPtr = 0;
	circle->writePtr = 0;
	circle->size = 0;
}


uint16_t CircularBuffer_verifyPacket(CircularBuffer circle){
    //uint16_t i, j;
    uint8_t temp[CIRCULAR_LEN];
    if(circle->size != CIRCULAR_LEN){ // wrong size
        printf("wrong: size is %d\n", circle->size);
        return 0;
    }
    CircularBuffer_getBuf(circle, temp, circle->size); //update temp
    if(temp[0]==HEAD_ID && temp[CIRCULAR_LEN-1]==TAIL_ID){ //valid messsage format
        return 1;
    }
    else{
        // printf("\nHead is \\%x, should be \\%x\n", temp[0], HEAD_ID);
        // printf("Tail is \\%x, should be \\%x\n", temp[CIRCULAR_LEN-1], TAIL_ID);
        // printf("wrong format\n");
        return 0;
    }
}

void CircularBuffer_print(CircularBuffer circle){
	uint16_t i, j;
	for(i = 0, j = circle->readPtr; i < circle->size; i++){
		printf("\\%x", circle->buffer[j]);
		j++;
		if(j >= CIRCULAR_LEN)
			j = 0;
	}
	printf("\r\n");
}

void CircularBuffer_delete(CircularBuffer* p){
	if( p!=NULL && *p!=NULL ){
		free(*p);
		*p = NULL;
	}
}


/* testing for re-sync
int main(void){
	circular_init();
	uint8_t temp0[] = {222,47,182,99,32,69,0,1,2,3,4,0}; //valid
	uint8_t temp1[] = {222,47,182,99,32}; //invalid - pkt first half
	uint8_t temp2[] = {69,0,1,2,3,4,0}; //invalid - pkt second half
	uint8_t temp3[] = {222,69,69,69,69,69,69,69,69,69,69,0}; //valid
    uint8_t temp4[] = {420}; //invalid - noise
	uint8_t buf[CIRCULAR_LEN];

	circular_append(temp0, CIRCULAR_LEN);
	//circular_append(temp1, CIRCULAR_LEN);
	circular_append(temp2, 6);
	circular_append(temp3, CIRCULAR_LEN);
	circular_append(temp4, 1);
	circular_append(temp4, 1);
	circular_append(temp4, 1);
	circular_append(temp0, CIRCULAR_LEN);
	circular_getBuf(buf, CIRCULAR_LEN);
    for(int i=0; i<CIRCULAR_LEN; i++){
	    printf("%d ", buf[i]);
    }
    if(verifyPacket()){
        printf("\n SUCCESS! \n");
    }
    else{
        printf("\n FAILURE! \n");
    }

	return 1;
}
*/
