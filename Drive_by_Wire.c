#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <stdio.h>
#include <stdlib.h>
#include "Serial.h"
#include "main.h"
#include "operation.h"
#include "CircularBuffer.h"
#include "CarValues.h"
#include "setup.h" //TODO
#include "LEDs.h"
#include "packet_builder.h"
#include <softPwm.h>
#include <math.h>
#include <string.h>

#define CHECKSUM_ERROR_LENGTH 99
#define MAX(a,b) (((a)>(b))?(a):(b))
#define PI 3.1415926

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

// Operation state variables //
static volatile InputMode inputMode = SYSTEMOFF; // TODO: SYSTEMOFF;
static volatile bool inMotion = false;
static volatile bool resetWasPressed = false;
static volatile bool systemOFF = false;
static CarState currentState = IDLE; //TODO: IDLE;
static volatile bool e_stop_pressed = false;

// Buffers //
static uint8_t circTemp[CIRCULAR_LEN * 3]; // used to access circular buffer contents
static CircularBuffer joystickCircBuf;
static CircularBuffer autonCircBuf;

/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

void DISPLAY(char* str){
    printf("%s\r\n",str);
	Serial_write_null(str, strlen(str));
}

void global_enable_on(void){
	digitalWrite(GLOBAL_EN, HIGH);
}

void global_enable_off(void){
	digitalWrite(GLOBAL_EN, LOW);
}

void play_error_tone(void){
	digitalWrite(ALARM, HIGH);
}

void stop_error_tone(void){
	digitalWrite(ALARM, LOW);
}

bool manual_input_detected(void){ // TODO: REMOVE
	return 0;
}

bool joystick_past_threshold(CarValues *gamepadRefValues) {
    if ( abs(gamepadRefValues->steeringAngle - 2500) > STEERING_INPUT_THRESHOLD || 
	gamepadRefValues->percentBrake > BRAKING_INPUT_THRESHOLD || 
	gamepadRefValues->percentThrottle > THROTTLE_INPUT_THRESHOLD) {
		printf("joystick takeover\n");
		return TRUE;
    } else return FALSE;
}

void set_speed_zero(CarValues *newVals, CarValues *prevVals){
    newVals->percentThrottle = 0;
    prevVals->percentBrake = 0;
}

void shut_off_pwm(void) {
	softPwmWrite(PWM0, 0);
	softPwmWrite(PWM1, 0);
	softPwmWrite(PWM2, 0);
	softPwmWrite(PWM3, 0);
}

void shut_off_peripherals(void){
	digitalWrite(LEFT_TURN, HIGH);
    digitalWrite(RIGHT_TURN, HIGH);
    digitalWrite(HEADLIGHT, HIGH);
    digitalWrite(HORN, HIGH);
}

int main(int argc, char * argv[]){
    uint32_t curTime = 0;
	uint8_t throttle_err;
	bool updateStuff = false;
    
	int32_t numErrorsGamepad = 0;
	int32_t numErrorsAuton = 0;

    CarValues* newVals = car_values_init();
    CarValues* prevVals = car_values_init();
	CarValues* gamepadRefVals = car_values_init();
	CarValues* autonRefVals = car_values_init();
	CarValues* cValsOff = car_values_init();
	cValsOff->steeringAngle = 2500;

    
    joystickCircBuf = CircularBuffer_init();
    autonCircBuf = CircularBuffer_init();
    
    GPIO_init();
	Serial_init();
    
    init_PID_loops();
    printf("Init stuff done\r\n");

    DISPLAY("DBW IS IDLE\0");

	refresh_all_adc();

	
    for(;;) {
        
		// autonomous_read(autonRefVals, autonCircBuf);
		joystick_read(gamepadRefVals, joystickCircBuf);

		if (curTime + 50 <= millis()) {
			if (digitalRead(SYS_OFF_BTN) == 1) inputMode = SYSTEMOFF;

			int gamepadTriggered = digitalRead(JOYSTICK_BTN);
			int autonTriggered = digitalRead(AUTON_BTN);
			if (gamepadTriggered && autonTriggered);
			else if (gamepadTriggered || autonTriggered) inputMode = gamepadTriggered ? GAMEPAD : AUTONOMOUS;
			curTime = millis();
			
			updateStuff = true;
			refresh_all_adc();
		}
	

        switch(currentState) {

	/**********************************************************************************
	* IDLE State: Vehicle stays here until a switch is pressed (indicated by inputMode)
	***********************************************************************************/

	case IDLE:
	
	if (inputMode == GAMEPAD || inputMode == AUTONOMOUS) {

	    if (inMotion == True) {
			currentState = IDLE;
			DISPLAY("CANNOT START DBW WHILE IN MOTION\0");
	    } //don't enable the system if the car is in motion

	    else currentState = SETUP;

	}

	break;

	/**********************************************************************************
	* SETUP State: Perform necessary actions to put vehicle in OPERATION state
	***********************************************************************************/
	
	case SETUP:
	    /*
	    // TODO
	    // verify that the mode select E-STOP is working
	    // DISPLAY("Press the systemOFF button!");
	    while (systemOFF == False); */
	    
	    if ((DAC_test() == SUCCESS) && (brake_test() == SUCCESS) && (steering_test() == SUCCESS)) {
			currentState = SOFTWARE_CONTROL;
			printf("steering & braking test passed\n");
			(inputMode == GAMEPAD) ? gamepad_LED() : auton_LED();
			global_enable_on();
			reset_estop_flag();
	    }
	    else {
			currentState = STARTUP_ERROR;
			DISPLAY("STARTUP ERROR OCCURED\0");
			play_error_tone();
			system_off_LED();
	    }

	    break;

	/**********************************************************************************
	* SOFTWARE_CONTROL State: The vehicle will remain in this state under nominal conditions
	***********************************************************************************/

	case SOFTWARE_CONTROL:

	    // if any e-stop was pressed, enter shutdown routine
	    if (estop_was_pressed() == true) {
			//update_peripherals(cValsOff);
			global_enable_off();
			shut_off_peripherals();
			shut_off_pwm();
			currentState = E_STOP_ERROR;
			DISPLAY("E_STOP PRESSED\0");
			play_error_tone();
			system_off_LED();
			break;
	    }

	    // if input mode channel pressed or manual override, switch to manual control
	    if (manual_override_detected()) {
			// printf("%d %d\n", get_brake0(), get_brake1());
			//update_peripherals(cValsOff);
			shut_off_peripherals();
			shut_off_pwm();
			currentState = MANUAL_CONTROL;
			inputMode = SYSTEMOFF;
			DISPLAY("MANUAL TAKEOVER DETECTED\0");
			global_enable_off();
			system_off_LED();
			break;
	    }

		if(inputMode == SYSTEMOFF) {
			//update_peripherals(cValsOff);
			shut_off_peripherals();
			shut_off_pwm();
			currentState = MANUAL_CONTROL;
			DISPLAY("SYSTEM OFF DETECTED\0");
			global_enable_off();
			system_off_LED();
			break;
		}
		

		if (get_autonomous_flag()) {
			CircularBuffer_getBuf(autonCircBuf, circTemp, PACKET_LEN);
				if (packet2values(circTemp, autonRefVals) > CHECKSUM_ERROR_LENGTH) {
					numErrorsAuton++;
					for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
					printf("\r\n");
					if (inputMode == AUTONOMOUS && numErrorsAuton > 4) {
						//update_peripherals(cValsOff);
						shut_off_peripherals();
						currentState = MID_OPERATION_ERROR;
						printf("Autonmous error occured\n");
						DISPLAY("MID-OPERATION ERROR OCCURED\0");
						shut_off_pwm();
						global_enable_off();
						play_error_tone();
						system_off_LED();
						break;
					}
				} else {
					numErrorsAuton = 0;
					//printf("steer: %d, throttle: %d, brake: %d\r\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);

				}

			CircularBuffer_clear(autonCircBuf);
	    }

	    // parse input from joystick controller 
	    if (get_joystick_flag()) {
			CircularBuffer_getBuf(joystickCircBuf, circTemp, PACKET_LEN);
				if (packet2values(circTemp, gamepadRefVals) > CHECKSUM_ERROR_LENGTH) {
					numErrorsGamepad++;
					for(uint8_t i = 0; i < PACKET_LEN; i++) printf("\\%x", circTemp[i]);
					printf("\r\n");
					if (inputMode == GAMEPAD && numErrorsGamepad > 4) {
						//update_peripherals(cValsOff);
						shut_off_peripherals();
						currentState = MID_OPERATION_ERROR;
						printf("Joystick error occured\n");
						DISPLAY("MID-OPERATION ERROR OCCURED\0");
						shut_off_pwm();
						global_enable_off();
						play_error_tone();
						system_off_LED();
						break;
					}
				} else {
					numErrorsGamepad = 0;
					//printf("steer: %d, throttle: %d, brake: %d\r\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);

				}

			CircularBuffer_clear(joystickCircBuf);
	    }

	    // check for gamepad override
	    if (inputMode == AUTONOMOUS && joystick_past_threshold(gamepadRefVals)) {
			inputMode = GAMEPAD;
			gamepad_LED();
			break;
	    }

		if (inputMode == GAMEPAD) {
			car_values_overwrite(newVals, gamepadRefVals);
		} else car_values_overwrite(newVals, autonRefVals);


		if (updateStuff == true && currentState == SOFTWARE_CONTROL) {
			// this is here cause arturo told me to put it here
			// refresh_all_adc();

			printf("steer: %d, throttle: %d, brake: %d\r\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);
			digitalWrite(GLOBAL_EN, HIGH);
			// Updating the DAC with the desired voltage
			// TO DO: add check with ADC?
			if (update_throttle(newVals) == FAILURE || (newVals->percentThrottle > 10 && abs(read_adc(0) * 148 / 100 - 11 - newVals->percentThrottle * 10) > 15)) {
				throttle_err = 0; // TODO: Increment
				if(throttle_err > 3){
					printf("%d, %d\n", read_adc(0) * 148 / 100 - 11, newVals->percentThrottle * 10);
					currentState = MID_OPERATION_ERROR;
					DISPLAY("MID-OPERATION ERROR OCCURED\0");
					//update_peripherals(cValsOff);
					shut_off_peripherals();
					shut_off_pwm();
					play_error_tone();
					system_off_LED();
					break;
				}
			} else throttle_err = 0;

			// Updating the extension of the brake actuator
			// TO DO: add check to see if connected
			if (update_braking(newVals) == FAILURE) {
				currentState = MID_OPERATION_ERROR;
				DISPLAY("MID-OPERATION ERROR OCCURED\0");
				//update_peripherals(cValsOff);
				shut_off_peripherals();
				shut_off_pwm();
				play_error_tone();
				system_off_LED();
				break;
			}

			// Updating the extension of the steering actuator
			// TO DO: add check to see if connected
			if (update_steering(newVals) == FAILURE) {
				currentState = MID_OPERATION_ERROR;
				DISPLAY("MID-OPERATION ERROR OCCURED\0");
				//update_peripherals(cValsOff);
				shut_off_peripherals();
				shut_off_pwm();
				play_error_tone();
				system_off_LED();
				break;
			}

			update_peripherals(newVals, prevVals);

			car_values_overwrite(prevVals, newVals);

			updateStuff = false;
		}


	    break;

	/**********************************************************************************
	* MANUAL_CONTROL State: The vehicle will remain in this state under nominal conditions
	***********************************************************************************/

	case MANUAL_CONTROL:
	
	    if (inputMode == AUTONOMOUS || inputMode == GAMEPAD) {
			if (inMotion) DISPLAY("CANNOT START DBW WHILE IN MOTION\0");
			else {
				currentState = SOFTWARE_CONTROL;
				(inputMode == GAMEPAD) ? gamepad_LED() : auton_LED();
			}
	    }

	    break;

	/**********************************************************************************
	* STARTUP ERROR State: The vehicle will remain in this state under nominal conditions
	***********************************************************************************/

	case STARTUP_ERROR:

	    if (inputMode == SYSTEMOFF) {
			currentState = IDLE;
			inputMode = SYSTEMOFF;
			car_values_reset(newVals);
			car_values_reset(prevVals);
			stop_error_tone();
	    }

	    break;

	/**********************************************************************************
	* MID-OPERATION ERROR State: The vehicle will remain in this state under nominal conditions
	***********************************************************************************/

	case MID_OPERATION_ERROR:

	    if (inputMode == SYSTEMOFF) {
			currentState = IDLE;
			inputMode = SYSTEMOFF;
			car_values_reset(newVals);
			car_values_reset(prevVals);
			stop_error_tone();
	    }

	    break;

	/**********************************************************************************
	* E_STOP ERROR State: The vehicle will remain in this state under nominal conditions
	***********************************************************************************/

	case E_STOP_ERROR:

	    if (inMotion) {
			set_speed_zero(newVals, prevVals);
			update_braking(newVals);
	    } 
	    else {
			if (inputMode == SYSTEMOFF) {
				shut_off_pwm();
				currentState = IDLE;
				inputMode = SYSTEMOFF;
				car_values_reset(newVals);
				car_values_reset(prevVals);
				stop_error_tone();
				reset_estop_flag();
			}
	    }
		if(updateStuff){
			newVals->percentBrake = 100;
			update_braking(newVals);
			updateStuff = false;
		}
	    break;

		
        }
    }

	//fclose(log);
    return 1;
}
