/*
 * LEDs.h
 *
 *  Created on: Mar 5, 2022
 *      Author: svenka12
 */

#ifndef LEDS_H_
#define LEDS_H_

#include <wiringPi.h>
#include "main.h"

void system_off_LED(void) {
	digitalWrite(SYS_OFF_LED, HIGH);
	digitalWrite(AUTON_LED, LOW);
	digitalWrite(JOYSTICK_LED, LOW);
}


void gamepad_LED(void) {
	digitalWrite(SYS_OFF_LED, LOW);
	digitalWrite(AUTON_LED, LOW);
	digitalWrite(JOYSTICK_LED, HIGH);
}
void auton_LED(void) {
	digitalWrite(SYS_OFF_LED, LOW);
	digitalWrite(AUTON_LED, HIGH);
	digitalWrite(JOYSTICK_LED, LOW);
}


#endif /* LEDS_H_ */


