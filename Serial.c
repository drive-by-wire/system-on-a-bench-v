/*
 * Serial.c
 * 
 * Created on: May 16, 2022
 *      Author: Arturo
 * 
 * Adapted from CSE121 with Prof. Varma
 */

#include <wiringPi.h>
#include <wiringSerial.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include "Serial.h"

#define RX_LEN 255

static struct termios serial;
static char* dev_id = "/dev/serial0"; // UART device identifier
static int fd;
static uint8_t rxBuffer[RX_LEN];
static uint8_t readIdx;
static uint8_t writeIdx;
static uint8_t circle[RX_LEN];

//static CircularBuffer rxCircle;


int8_t Serial_init(void){
    readIdx = 0;
    writeIdx = 0;
    
    for(uint8_t i; i < RX_LEN; i++) {
        circle[i] = 0;
        rxBuffer[i] = 0;
    }

    for(uint8_t i = 0; i < RX_LEN; i++) rxBuffer[i] = 0;

    fd = open(dev_id, O_RDWR | O_NOCTTY | O_NDELAY);
    // O_RDWR indicates the device needs to both written and read.
    // O_NOCTTY states that the device is not used as a console.
    // O_NDELAY indicates that read and writes should be nonblocking.

    if (fd == -1){ // Open failed
	perror(dev_id);
	return -1;
    }

    // Get UART configuration
    if (tcgetattr(fd, &serial) < 0){
	perror("Getting configuration");
	return -1;
    }

    // Set UART parameters in the termios structure
    serial.c_iflag = 0;
    serial.c_oflag = 0;
    serial.c_lflag = 0;
    serial.c_cflag = BAUDRATE | CS8 | CREAD | PARENB | PARODD;
    // Speed setting + 8-bit data + Enable RX + Enable Parity + Odd Parity

    serial.c_cc[VMIN] = 0; // 0 for Nonblocking mode
    serial.c_cc[VTIME] = 0; // 0 for Nonblocking mode

    // Set the parameters by writing the configuration
    tcsetattr(fd, TCSANOW, &serial);

    return 1;
}

int8_t Serial_isRxEmpty(void){
    return readIdx == writeIdx;
}

int8_t Serial_update(void){
    int8_t readBytes = read(fd, rxBuffer, sizeof(rxBuffer));
    //if(readBytes) printf("%d\r\n", readBytes);

    if(readBytes < 0){
        //perror("Invalid Read");
        return -1;
    }
    
    for (uint8_t i = 0; i < readBytes; i++){
        circle[writeIdx++] = rxBuffer[i];
        if(writeIdx == readIdx){
            readIdx++;
            readIdx %= RX_LEN;
        }
        writeIdx %= RX_LEN;
    }
    
    return readBytes;
}

// TODO : Make this more like a circular buffer
// Currently size does nothing
int8_t Serial_read(uint8_t* buffer, uint8_t size){
    for(uint8_t i = 0; i < size; i++){
        if(readIdx == writeIdx)
            return i;
        buffer[i] = circle[readIdx++];
        readIdx %= RX_LEN;
    }
    return size;
}

int8_t Serial_write(uint8_t* buffer, uint8_t size){
    int8_t writeBytes = write(fd, buffer, size);
    if(writeBytes < 0){
        perror("Invalid Write");
        return -1;
    }
    return writeBytes;
}

int8_t Serial_write_null(uint8_t* buffer, uint8_t size){
    uint8_t str[] = "\0";
    int8_t writeBytes = write(fd, buffer, size);
    if(writeBytes < 0){
        perror("Invalid Write");
        return -1;
    }
    writeBytes = write(fd, str, 1);
    if(writeBytes < 0){
        perror("Invalid Write");
        return -1;
    }
    return writeBytes;
}
