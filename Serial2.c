#include <wiringPi.h>
#include <wiringSerial.h>
#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#define BAUDRATE B115200

static int fd;
static struct termios serial;
static char* dev_id = "/dev/serial0"; // UART device identifier

int8_t Serial_init(void){
    wiringPiSetup();
    fd = serialOpen(dev_id, 115200);
    if(fd < 0){
        printf("Serial error\r\n");
        return -1;
    }
    //serial.c_cflag = BAUDRATE | CS8 | CREAD | PARENB | PARODD;
    //tcsetattr(fd, TCSANOW, &serial);
    tcgetattr (fd, &serial) ;   // Read current options
    serial.c_cflag &= ~CSIZE ;  // Mask out size
    // serial.c_cflag |= CREAD;
    serial.c_cflag |= CS8 ;     // Or in 8-bits
    serial.c_cflag |= PARENB ;  // Enable Parity - even by default
    //serial.c_cflag |= B115200;
    serial.c_cflag |= PARODD;
    tcsetattr (fd, TCSANOW, &serial) ;   // Set new options
    return 1;
}

int8_t Serial_update(void){
    return 0;
}

int8_t Serial_read(uint8_t* buffer, uint8_t size){
    return 0;
}

int8_t Serial_write(uint8_t* buffer, uint8_t size){
    char temp[] = "Hello\0";
    serialPuts(fd, temp);
    return 1;
}

int8_t Serial_isRxEmpty(void){
    return 0;
}

int main(int argc, char * argv[]){
    Serial_init();
    wiringPiSetup();

    char *temp = "Hello World\0";

    for (;;){
        //serialPuts(fd, temp);
        serialPutchar(fd, '3');
        serialPutchar(fd, '2');
        delay(10);
    }
    return 1;
}