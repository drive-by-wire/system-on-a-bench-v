
// Written by Sutter Lum
// Last updated: 5/21/2022
#include <stdint.h>
#include <stdio.h>
#include "packet_builder.h"
#include "crc32.h"

#define HEAD_ID 222 //8-bit number identifying start of message - 0xDE in hex
#define TAIL_ID 0 //8-bit number identifying end of message - 0x00 in hex
#define CHECK_LEN 6

void cmd2packet(uint8_t *packet, CarValues *cmd, uint8_t errorCode){
    packet[0] = HEAD_ID; //unique head ID
    packet[1] = (cmd->steeringAngle >> 8) & 0xFF;
    packet[2] = (cmd->steeringAngle) & 0xFF;
    packet[3] = (cmd->percentBrake) & 0xFF;
    packet[4] = (cmd->percentThrottle) & 0xFF;
    packet[5] = packet[5] | ((cmd->horn) & 0x01);
    packet[5] = packet[5] | ((cmd->reverse << 1) & 0x02);
    packet[5] = packet[5] | ((cmd->headlights << 2) & 0x04);
    packet[5] = packet[5] | ((cmd->ESTOP << 3) & 0x08);
    packet[5] = packet[5] | ((cmd->leftTurn << 4) & 0x10);
    packet[5] = packet[5] | ((cmd->rightTurn << 5) & 0x20);
    packet[6] = errorCode & 0xFF;
    

    uint8_t buffer[CHECK_LEN*sizeof(uint8_t)];
    int i = 0;
    for (i=0; i<CHECK_LEN; i++){
        buffer[i] = packet[i+1];
    }
    uint32_t checksum = crc32_checksum(buffer, CHECK_LEN*sizeof(uint8_t));
    packet[7] = (checksum >> 24) & 0xFF;
    packet[8] = (checksum >> 16) & 0xFF;
    packet[9] = (checksum >> 8) & 0xFF;
    packet[10] = checksum & 0xFF;
    packet[11] = TAIL_ID; //unique Tail ID
}

uint8_t packet2values(uint8_t *packet, CarValues *curValues){
    //ignore packet[0] and packet[11] - head and tail IDs
    curValues->steeringAngle = (packet[1] << 8) + packet[2];
    curValues->percentBrake = packet[3];
    curValues->percentThrottle = packet[4];
    curValues->horn = packet[5] & 0x01;
    curValues->reverse = (packet[5] & 0x02) >> 1;
    curValues->headlights = (packet[5] & 0x04) >> 2;
    curValues->ESTOP = (packet[5] & 0x08) >> 3;
    curValues->leftTurn = (packet[5] & 0x10) >> 4;
    curValues->rightTurn = (packet[5] & 0x20) >> 5;
    uint8_t errorCode = packet[6];

    uint32_t givenCS = (packet[7] << 24) + (packet[8] << 16) + (packet[9] << 8) + packet[10];
    uint8_t buffer[CHECK_LEN*sizeof(uint8_t)];
    int i = 0;
    for (i=0; i<CHECK_LEN; i++){
        buffer[i] = packet[i+1];
    }

    uint32_t calcCS = crc32_checksum(buffer, CHECK_LEN*sizeof(uint8_t));
    //printf("Given Checksum = %d \r\n", givenCS);
    //printf("Calculated Checksum = %d \r\n", calcCS);

    if (givenCS != calcCS){
        errorCode += 100; //any error code over 100 indicates checksum mismatch + regular error(0-99)
    }
    return errorCode; //returns the error code
}

/*int main(void){
    CarValues car;
    uint8_t packet[] = {222, 9, 0xC4, 0, 0, 0, 0, 0xAF, 0x8F, 0x29, 0x82, 0};
    uint8_t error = packet2values(packet, car);
    printf("Error code is = %d\r\n", error);

    uint8_t buffer[] = {9, 0xC4, 0, 0, 0, 0};
    uint8_t buffer2[] = {9, 0xC4, 0, 0, 0, 0};
    uint32_t test1 = crc32_checksum(buffer, 6*sizeof(uint8_t));
    uint32_t test2 = crc32_checksum(buffer2, 6*sizeof(uint8_t));
    printf("test1 Checksum = %d \r\n", test1);
    printf("test2 Checksum = %d \r\n", test2);
    return 69;
}*/
