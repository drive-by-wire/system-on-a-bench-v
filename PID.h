/*
 * PID.h
 *
 *  Created on: May 3, 2022
 *      Author: artur
 */

#ifndef INCLUDE_PID_H_
#define INCLUDE_PID_H_

#include <stdint.h>

#define TICKS_PER_ROT 819
#define SATURATION TICKS_PER_ROT
#define TS 50 // Time step in milliseconds
#define P 1.8 // Proportional gain
#define I 0.06 // Integral gain
#define D 0 // Derivative gain

typedef struct PIDObj *PID;

/*
 * Description: Initializes a PID object
 * Arguments:
 *   p - Proportional Gain
 *	 i - Integral Gain
 *   d - Derivative Gain
 *   ts - Time step
 *   currentPos - current plant position in ticks
 *   saturation - actuator control effort saturation limit in ticks
 * Return:
 *   A pointer to a PID object
 */
PID PID_init(float k_p, float k_i, float k_d, uint16_t ts, uint16_t currentPos, uint16_t saturation);

/*
 * Description: Updates the commanded position
 * Arguments:
 *   pid - the PID object to update
 *   command - the desired position in ticks
 * Return:
 *   None
 */
void PID_setPosition(PID pid, uint16_t command);

/*
 * Description: Performs the digital PID calculations to get
 * 				the actuator control effort
 * Arguments:
 *   pid - the PID object to update
 *   curPos - the current plant position in ticks
 * Return:
 *   the control effort for that time step
 */
int8_t PID_update(PID pid, uint16_t curPos);

/*
 * Description: Get the current control effort
 * Arguments:
 *   pid - the PID object to read from
 * Return:
 *   The current control effort
 */
int8_t PID_getControlEffort(PID pid);

/*
 * Description: Frees the allocated PID object
 * Arguments:
 *   p - pointer to the PID pointer to be freed
 * Return:
 *   None
 */
void PID_delete(PID* p);

#endif /* INCLUDE_PID_H_ */
